FROM ubuntu:latest
#root
RUN apt-get update \
&& apt-get install -y nano \
&& rm -rf /var/list/apt/lists/*
#root
RUN groupadd -r user && useradd -r -g user user
#root
ENV FILE_DIR=/home/app
#root
RUN mkdir -p $FILE_DIR

#root
WORKDIR $FILE_DIR
ARG RAS=RASChapter
RUN echo "bienvenidos a ras $RAS " > example2.txt

#root
#USER user

COPY app .