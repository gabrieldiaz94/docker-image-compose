FROM osrf/ros:iron-desktop-full

SHELL [ "/bin/bash","-c" ]

RUN apt-get update \
&& apt-get install -y nano \
&& rm -rf /var/list/apt/lists/*

ENV RAS_WS=/root/ras_ws_1
RUN mkdir -p $RAS_WS/src
WORKDIR $RAS_WS

#RUN ROS and build
RUN source /opt/ros/$ROS_DISTRO/setup.bash \ 
&& colcon build \
&& source install/setup.bash