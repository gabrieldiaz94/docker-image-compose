## Getting started

Clone this repository

```
git clone https://gitlab.com/gabrieldiaz94/docker-image-compose.git
```

## Build, Run and Exec the container

- Run the container inside the deployment folder, for easy use of this container run the bash file

```
source run_image.sh
```